import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

    private double RPart, IPart, JPart, KPart;
    final static double THRESHOLD = .00001;


   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      RPart = a;
      IPart = b;
      JPart = c;
      KPart = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return RPart;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return IPart;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
       return JPart;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
       return KPart;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(getRpart());
      if (getIpart() >= 0) {
          sb.append("+");
      }
      sb.append(getIpart() + "i");
       if (getJpart() >= 0) {
           sb.append("+");
       }
       sb.append(getJpart() + "j");
       if (getKpart() >= 0) {
           sb.append("+");
       }
       sb.append(getKpart() + "k");
       return sb.toString();
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
       double r, i, j, k;
       ArrayList<Double> values = new ArrayList<Double>();
       if (s.charAt(s.length()-1) == 'k') {
           int n = s.indexOf('j');
           if (n != -1) {
               k = Double.parseDouble(s.substring(n + 1, s.length() - 1));
               int m = s.indexOf('i');
               if (m != -1) {
                   j = Double.parseDouble(s.substring(m+ 1, n));
                   int firstPlus = s.indexOf('+');
                   int firstMinus = s.indexOf('-');
                   if (s.charAt(0) == '-') {
                       firstMinus = s.substring(1).indexOf('-');
                   }
                   if (firstMinus > 0 && firstPlus > 0) { // IN CAE BOTH OPERANDS ARE PRESENT IN
                       if (firstPlus < firstMinus) { // + comes before -
                           r = Double.parseDouble(s.substring(0, firstPlus));
                           i = Double.parseDouble(s.substring(firstPlus + 1, m));
                       } else {
                           r = Double.parseDouble(s.substring(0, firstMinus));
                           i = Double.parseDouble(s.substring(firstMinus, m));
                       }
                   } else if (firstMinus > 0 && firstPlus == - 1) { // IN CASE THERE ARE ONLY MINUSES.
                       r = Double.parseDouble(s.substring(0, firstMinus));
                       i = Double.parseDouble(s.substring(firstMinus + 1, s.indexOf('i')));
                   } else if (firstPlus >0 && firstMinus ==-1) { // IN CASE THERE ARE ONLY PLUSES.
                       r = Double.parseDouble(s.substring(0, firstPlus));
                       i = Double.parseDouble(s.substring(firstPlus + 1, m));
                   } else {
                       throw new IllegalArgumentException( s + " is an invalid expression." );
                   }
               } else {
                   throw new IllegalArgumentException("i el is not present in the formula " + s);
               }
           } else {
               throw new IllegalArgumentException("j el is not present in the formula " + s);
           }

       } else {
           throw new IllegalArgumentException("k el is not present in the formula " + s);
       }
       return new Quaternion(r, i, j, k);
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
       double a = getRpart();
       double b = getIpart();
       double c = getJpart();
       double d = getKpart();
      return new Quaternion(a, b, c, d);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return Math.abs(getRpart()) < THRESHOLD && Math.abs(getIpart()) < THRESHOLD
              && Math.abs(getJpart()) < THRESHOLD && Math.abs(getKpart()) < THRESHOLD;
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() { ;
       return new Quaternion(getRpart(), getIpart() * (- 1), getJpart() * (- 1), getKpart() * (- 1));
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
       return new Quaternion(getRpart() * ( - 1), getIpart() * (-1), getJpart()*(-1), getKpart() * (-1));
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {

       return new Quaternion(getRpart() + q.getRpart(), getIpart() + q.getIpart(),
               getJpart() + q.getJpart(), getKpart() + q.getKpart());
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
       double newRp = getRpart() * q.getRpart() - getIpart() *q.getIpart() - getJpart() * q.getJpart()
               - getKpart() *q.getKpart();
       double newIp = getRpart() * q.getIpart() + getIpart() * q.getRpart() + getJpart() * q.getKpart()
               - getKpart() * q.getJpart();
       double newJp = getRpart() * q.getJpart() - getIpart() * q.getKpart() + getJpart()*q.getRpart()
               + getKpart() * q.getIpart();
       double newKp = getRpart() * q.getKpart() + getIpart() * q. getJpart() - getJpart() * q.getIpart()
               + getKpart() * q.getRpart();

       return new Quaternion(newRp, newIp, newJp, newKp);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(getRpart() * r, getIpart() * r, getJpart() * r, getKpart() * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
       if (isZero()) {
           throw new RuntimeException("All elements are 0, cannot inverse. ");
       }
       double newRp = getRpart() / (norm() * norm()) ;
       double newIp = (getIpart() * (- 1)) / (norm() * norm());
       double newJp = (getJpart() * (- 1)) / (norm() * norm());
       double newKp = (getKpart() * (- 1)) / (norm() * norm());
      return new Quaternion(newRp, newIp, newJp, newKp);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return q.opposite().plus(this);

   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
       if (q.isZero()) {
           throw new RuntimeException("Cannot divide with 0.");
       }
      return times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
       if (q.isZero()) {
           throw new RuntimeException("Cannot divide with 0.");
       }
      return q.inverse().times(this);
   }


   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
       Quaternion q = (Quaternion) qo;
      return Math.abs(q.getRpart() - getRpart()) <  this.THRESHOLD
              && Math.abs(q.getIpart() - getIpart()) <  this.THRESHOLD
                && Math.abs(q.getJpart() - getJpart()) <  this.THRESHOLD
              && Math.abs(q.getKpart() - getKpart()) <  this.THRESHOLD;
   }


   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
       Quaternion temp1 = times(q.conjugate());
       Quaternion temp2 = q.times(conjugate());
       Quaternion temp = temp1.plus(temp2);
      return temp.times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(RPart, IPart, JPart, KPart);
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(getRpart() * getRpart() + getIpart() * getIpart() + getJpart() * getJpart()
              + getKpart() * getKpart());
   }


   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
       System.out.println(valueOf("-1+1i+2j-2k"));
       System.out.println(arv1);
       Quaternion f = new Quaternion (3., -2., 1., 3.);
       System.out.println(f);
       System.out.println(valueOf("3.0-2.0i+1.0j+3.0k"));
       Quaternion m = new Quaternion (2., 5., 7., 9.);
       System.out.println(m);
       System.out.println(valueOf("2.0+5.0i+7.0j+9.0k"));
       f = new Quaternion (-3., -2., -10., -30.);
       System.out.println(f);
       System.out.println(valueOf("-3.0-2.0i-10.0j-30.0k"));


      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));

   }
}
// end of file
